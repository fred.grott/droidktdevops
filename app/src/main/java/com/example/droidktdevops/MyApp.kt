/* Copyright 2018 Fred Grott

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.example.droidktdevops
import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex

class MyApp : Application() {

    val Context.myApp: MyApp
        get() = applicationContext as MyApp

     override fun onCreate() {
        super.onCreate()
    }

    override fun attachBaseContext(base: Context) {

        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}