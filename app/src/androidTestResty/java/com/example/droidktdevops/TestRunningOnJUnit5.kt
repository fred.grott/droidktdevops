/* Copyright 2018 Fred Grott

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.example.droidktdevops

import android.app.Activity
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import com.jraska.falcon.FalconSpoonRule
import org.junit.Rule

import com.example.droidktdevops.HelloKotlin
import androidx.test.rule.ActivityTestRule
import com.example.droidktdevops.Utils.screenShot




// note test robot porject pattern slightly wrong need to use object class as fun is normally
// static in a kotlin class
class TestRunningOnJUnit5 {

    @get:Rule
    val mActivityTestRule: ActivityTestRule<HelloKotlin> = ActivityTestRule(HelloKotlin::class.java)

    @get:Rule
    val falconSpoonRule = FalconSpoonRule()



    @Test
    fun junit5() {
        Assertions.assertEquals(4, 2 + 2)
        screenShot(falconSpoonRule, "loginMissingEmailPassword")
    }
}