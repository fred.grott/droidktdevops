![devops](devopsandroid.jpeg)

# DroidKTDevops

AndroidDevOps is not just the build gradle stuff found in this project but the overall distinct choices in
such things whether to use isntrumented testing or not, how to build interfaces to use
contexts for better UI testing, the correct app arch layer abstraaction choice, and the
correct state implementation choices so that the android app becomes a fully 100 percent testable
app. Or in short words a complex and powerfull android app with a very extremely
low bug count and the mark of a senior android app engineer.

# TODO

rxjava kotlin utilities library ie debug utilities

docker image and non shared gitlab ci config

yeoman setup of this project tools

mvi boilerplate





# Libraries used

RxJava

Androidx KTX

Androidx

Kotlin

# Plugins Used

Dokka

Detekt

Spotless


Jacoco

BuildTimeTracker

Junit5

Godot

Build Properties

Dexcount

# Security Best Practices

Release builds ARE NEVER EVER cnfigured to run on zci servers and
are set by default to generate a build error. Web apis are treated
similar in that one should use a secrets.xml res file in buildConfig
field for the build type release and staggingRelease and a default dummy file
with dummy vars to mock on debug builds.

The reason for web apis in res file inputed in buildConfig field as the
proguard obfuscates res files correctly where you cannot get at the
plain text easily as opposed to inserting same values directly in
code.

Yes, being paranoid is hellpful at times.


# Versioning

I use a minSdk prefix in my versionCode name to eease effort to track
versions via crash analytics.

The versioning is done the lazy way and not automated per commit as a modification
of Jake Wharton's (Square) best practices as it than never conflicts with other
contributors forking and doing prs, etc.


# Testing

Pitest mutation regression testing is executed as part of the debug build.
Legacy junit4 tdd instrumented testing for edge use cases is part of the debug build.
Junit5 TDD and Junit5 BDD are executed as their separate flavors.

Code coverage is setupusing jacoco with the tasks preconfigured for
every product flavor and integrated so that when tests are executed on
a product flavor the jacoco reprot gets generated.






![gitlab](gitlab.jpg)
# Gitlab Runners

One needs to use sdkmanager commandlitne to get the packagelist for the
gitlab ci config, see my example output ie sdk-package-list.txt

Basic deep dive on KVM enabled machines you will not get docker privilege mode access to
hardware acceleration as its a security violation on shared runners.
So you have two options, on shared runners set emulator with no
acceleration by default or shell out for your own VM and make
sure when your docker image launches that you use the extra
privileged mode command flags.

Base gl doc I am working from is this one:
https://about.gitlab.com/2017/11/20/working-with-yaml-gitlab-ci-android/

## Shared Runner


Make sure to set the no acel option in your config

Yes, it will be slow. Switch TDD and BDD testing to separate flavors to
stage separately to avoid some of the slowness.

![docker](docker.jpg)
## Own Hosted Runner

Use this author's method for creating a snapshot emaultor docker image as
he tells how to do the privileged mode launch of docker to get access
to KVM features for hardware acceleration:

https://github.com/yorickvanzweeden/android-ci

As far as additional gl runner confgis to set, do the cache settings
as explained here in this SO question:

https://stackoverflow.com/questions/34162120/gitlab-ci-gradle-dependency-cache/36050711

## CI Deep Dive

Most CI severs run the docker image in such a way that such things as
gradle caching, extra machine function access, etc outside the docker image.
Some of it can be accessed via bash scripting in the CI server config
setup file. Other stuff such as KVM enabling and access cannot be
accessed via bash scripting in the ci config script.

Also remember you can export in the CI docker container between the runner
and the gradle buidl script, for example setting the versionname apk name of artifact
see this SO question:

https://stackoverflow.com/questions/39752984/is-it-possible-to-use-a-gradle-config-variable-in-my-gitlab-ci-file

# License

[Apache 2.0 License](LICENSE.md)

# Credits

Created by Fred Grott
To contact fred DOT grott AT gmail DOT com